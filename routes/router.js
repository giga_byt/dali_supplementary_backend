const express = require('express');
const DaliDataAccess = require('../controllers/DaliDataAccess.js');




var router = express.Router();
let dataAccessor = new DaliDataAccess('test', 'test');

router.get('/users', function(req, res) {
    res.send(dataAccessor.query(req.query, 'users'));
});

router.get('/data', function(req, res) {
    res.send(dataAccessor.query(req.query, 'data'));
});

router.get('/users/count', function(req, res) {
    res.send(dataAccessor.getCount(req.query, 'users'));
});

router.get('/data/count', function(req, res) {
    res.send(dataAccessor.getCount(req.query, 'data'));
});

router.post('/users/add', function(req, res) {
    dataAccessor.addData(req.body, 'users');
    res.status(200).send('200');
});

router.post('/data/add', function(req, res) {
    dataAccessor.addData(req.body, 'data');
    res.status(200).send('200');
});

router.get('/users/delete', function(req, res) {
    dataAccessor.delete(req.query, 'users');
    res.status(200).send("OK");
});

router.get('/data/delete', function(req, res) {
    dataAccessor.delete(req.query, 'data');
    res.status(200).send("OK");
});

module.exports = router;