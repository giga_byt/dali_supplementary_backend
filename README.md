# Usage:
`npm i --save` to install necessary node modules.

`npm run start` to start the server at `localhost:3000`.

Currently, there is a basic CRUD interface implemented.

The routes are as follows (with `{data-type}` = one of `users`, `data`):
  * `/{data-type}`, GET request which filters by query-string params
  * `/{data-type}/delete`, GET request which deletes according to query-string params
  * `/{data-type}/add`, POST which adds the request body into the database
  * `/{data-type}/count`, GET request which returns a count according to matching query-string params