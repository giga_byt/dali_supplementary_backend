const fs = require('fs');

const nameddata = require('../data/dali_named.json');
const anondata = require('../data/dali_anon.json');

class DaliDataAccess {
    constructor(user, password) {
        /** In a real database, would store credentials */
        this.user = user;
        this.password = password;

        /** Load the database for this faux database simulator*/
        this.named_data = nameddata;
        this.anon_data = anondata;
    }

    /** Queries data for parameters with object containing filters
     * of the form 
     *      {
     *          key: value   
     *      }
     * If there are no filters supplied, gives entire database.
     * 
     * Mode decides which database to query.
     */
    query(param_object, mode) {
        let query = [];
        let currentData;
        currentData = (mode == 'users' ? this.named_data : this.anon_data);
        if(!param_object || Object.keys(param_object).length == 0){
            return currentData;
        }
        currentData.forEach(entry => {
            for(let searchProp in param_object){
                if(entry.hasOwnProperty(searchProp) && entry[searchProp] == param_object[searchProp]){
                    query.push(entry);
                }
            }
        });
        return query;
    }

    /** Deletes data for parameters with object containing filters
     * of the form 
     *      {
     *          key: value   
     *      }
     * If there are no filters supplied, does nothing.
     * 
     * Mode decides which database to query.
     */

    delete(param_object, mode){
        let currentData;
        currentData = (mode == 'users' ? this.named_data : this.anon_data);
        if(!param_object || Object.keys(param_object).length == 0){
            return;
        }

        for(let i = currentData.length; i >= 0; i--){
            let cobj = currentData[i];
            for(let searchProp in param_object) {
                if(cobj.hasOwnProperty(searchProp) && cobj[searchProp] == param_object[searchProp]){
                    currentData.splice(i, 1);
                    this.saveData();
                }
            }
        }
    }

    getCount(param_object, mode){
        let query = this.query(param_object, mode);
        return query.length;
    }

    addData(data_obj, mode) {
        if(!data_obj || Object.keys(data_obj).length == 0){
            return;
        } else {
            if(mode == 'users'){
                this.named_data.push(data_obj);
                this.saveData();
            } else {
                this.anon_data.push(data_obj);
                this.saveData();
            }
        }
    }

    saveData(){
        fs.writeFile('./data/dali_named.json', JSON.stringify(this.named_data, null, 2));
        fs.writeFile('./data/dali_anon.json', JSON.stringify(this.anon_data, null, 2));
    }
};

module.exports = DaliDataAccess;