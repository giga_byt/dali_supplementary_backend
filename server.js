/** imports */
const express = require('express');

const routes = require('./routes/router.js');


/** app init */
app = express();
app.use('/', routes);
app.use(express.json());

app.listen(3000, function() {
    console.log('listening on 3000')
});